
import java.io.*; 
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;


public class IMC {
	
	
	 private static final String VIRGULA = ";";
     static String dadobruto = "";      
    
   
 public static void main(String[] args) throws Exception {
     
     //instanciar � criar um objeto 
     
     // instanciando a classe que faz a leitura de csv
     
     BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\caique\\dataset.csv")));
      
     //fazer leitura antecipada da primeira linha que � o cabe�alho para que nao seja usado durante o calculo
     reader.readLine();

     //criando vari�vel para cada linha
     String linha = null;
     
     //fazendo a leitura dos restantes das linhas exceto a primeira (cabe�alho)
     
     //criando array list q vai guardar as informa�os modificadas do csv
    ArrayList<String>  documento = new ArrayList<String> ();       

     while ((linha = reader.readLine()) != null) {
         
         // retirando ponto e virgula que separam as colunas e colocando cada posi�ao do array
         String[] dadosUsuario = linha.split(VIRGULA);
       
         //trocando as virgulas por pontos, para que o calculo e a convers�o seja possivel.
                 
         String dado1 = dadosUsuario[2].replaceAll(",", "." ); 
     
        String dado2 = dadosUsuario[3].replaceAll(",", "." ); 
                
         // convertendo string para double para efetuar calculo
                          
         double peso = Double.parseDouble(dado1);        
       double altura = Double.parseDouble(dado2);
       
       //calculo do imc         
         double imc = peso /(altura*altura) ;
         
         //formatando a saida do calculo para receber 4 digitos separados por ponto
         String valorFormatado = new DecimalFormat("##.00").format(imc);
                     
         //System.out.println(Arrays.toString(dadosUsuario));
         
         //dado bruto para exportar no txt
         
                    //.toUpperCase() � o metodo para deixar texto em caixa alta    
    dadobruto = dadosUsuario[0].toUpperCase()+" "+dadosUsuario[1].toUpperCase()+" "+valorFormatado;     
    
    //adicionando as informa�oes dentro do array list
    documento.add(dadobruto);
                
      //impressao do resultado como foi solicitado
      
      //.toUpperCase() � o metodo para deixar texto em caixa alta
        System.out.println(dadosUsuario[0].toUpperCase()+" "+dadosUsuario[1].toUpperCase()+" "+valorFormatado);                   
         
         System.out.println("--------------------------");
          
     }
     
      BufferedWriter bf=null;
		FileWriter fileWriter=null;
		try {
			File file = new File("C:\\caique\\CaiqueDeJesusVillela.txt");
			fileWriter = new FileWriter(file);
			bf= new BufferedWriter(fileWriter);
                     
                     //inserindo as informa�oes do arraylist dentro do documento de texto
                     for(int i = 0; i < documento.size(); i++) {
           bf.write((String) documento.get(i)+"\n");
     }                    
		 

		} catch (IOException e) {

			e.printStackTrace();
		}finally{
			bf.close();
			fileWriter.close();
		}

     reader.close();
        
 }
	
	
	

}
